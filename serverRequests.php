<?php
/**
 * Created by PhpStorm.
 * User: Sean
 * Date: 24/02/2015
 * Time: 11:22
 */
 
	if(isset($_GET['userDetails'])){
		$user = $_GET['userDetails'];
		$json = file_get_contents('https://hummingbird.me/api/v1/users/' . $user);
		echo $json;
	}

	if(isset($_GET['userLibrary'])){
		$user = $_GET['userLibrary'];
		$lib = file_get_contents('https://hummingbird.me/api/v1/users/' . $user . '/library');
		echo $lib;
	}
?>
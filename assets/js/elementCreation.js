function createH1(h1Id, h1Class, h1InnerHTML){
	var h1 = document.createElement("h1");
	if(h1Id) h1.setAttribute("id",h1Id);
	if(h1Class) h1.setAttribute("class",h1Class);
	if(h1InnerHTML) h1.innerHTML = h1InnerHTML;

	return h1;
}

function createH2(h2Id, h2Class, h2InnerHTML){
	var h2 = document.createElement("h2");
	if(h2Id) h2.setAttribute("id",h2Id);
	if(h2Class) h2.setAttribute("class",h2Class);
	if(h2InnerHTML) h2.innerHTML = h2InnerHTML;

	return h2;
}

function createH3(h3Id, h3Class, h3InnerHTML){
	var h3 = document.createElement("h3");
	if(h3Id) h3.setAttribute("id",h3Id);
	if(h3Class) h3.setAttribute("class",h3Class);
	if(h3InnerHTML) h3.innerHTML = h3InnerHTML;

	return h3;
}

function createDiv(divId, divClass){
	var div = document.createElement("div");
	if(divId) div.setAttribute("id",divId);
	if(divClass) div.setAttribute("class", divClass);
		
	return div;
}

function createImg(imgId, imgClass, imgSrc){
	var img = document.createElement("img");
	if(imgId) img.setAttribute("id",imgId);
	if(imgClass) img.setAttribute("class", imgClass);
	if(imgSrc) img.setAttribute("src", imgSrc);

	return img;
}

function createButton(btnId, btnClass, btnOnClick, btnInnerHTML){
	var btn = document.createElement("button");
	if(btnId) btn.setAttribute("id",btnId);
	if(btnClass) btn.setAttribute("class", btnClass);
	if(btnOnClick) btn.setAttribute("onclick",btnOnClick);
	if(btnInnerHTML) btn.innerHTML = btnInnerHTML;

	return btn;
}

function createPara(pId, pClass, pInnerHTML){
	var p = document.createElement("p");
	if(pId) p.setAttribute("id",pId);
	if(pClass) p.setAttribute("class",pClass);
	if(pInnerHTML) p.innerHTML = pInnerHTML;

	return p;
}

function createHR(){
	var hr = document.createElement("hr");
	return hr;
}
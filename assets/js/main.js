
function getUser(){
	var username = document.getElementById("username").value;
	document.getElementById("username").value = "";

	var username = "ExodusErratacus";
	$.ajax({
		type: 'GET',
		url: 'serverRequests.php',
		data: { userDetails: username },
		success: function(data){
			if(data){
				console.log(JSON.parse(data));
				showUserProfile(data);
				getLibrary(username);
			} else
				alert("Invalid Username!"); //TODO nicer visual error.
		},
	})
}

function getLibrary(username){
	$.ajax({
		type: 'GET',
		url: 'serverRequests.php',
		data: { userLibrary: username },
		success: function(data){
			document.getElementsByClassName("loader")[0].setAttribute("style","display:none")
			console.log(JSON.parse(data));
			showUserLibrary(JSON.parse(data));
		}	
	})
}

function showUserProfile(userData){
	var json = JSON.parse(userData);

	var name = json.name;
	var avatar = json.avatar;
	var banner = json.cover_image;
	var waifuGen = json.waifu_or_husbando;
	var waifu = json.waifu;
	var waifu_anime = json.waifu_slug;
	var bio = json.about;
	var laifuSpent = calculateLife(json.life_spent_on_anime);

	document.getElementById("user-Avatar").setAttribute("src",avatar);
	document.getElementById("user-Banner").setAttribute("src",banner);

	document.getElementById("user-Name").innerHTML = '<strong>User: </strong>' + name;
	document.getElementById("user-Waifu").innerHTML = '<strong>' + waifuGen + ': </strong>' + waifu + " from " + formatAnimeTitle(waifu_anime);
	document.getElementById("user-Bio").innerHTML = '<strong>About Me: </strong>' + bio;
	document.getElementById("user-LaifuSpent").innerHTML = '<strong> Watched: </strong>' + laifuSpent +  ' of anime.';
	document.getElementById("userData-LineBreak").setAttribute("style","display:block");
	document.getElementById("userData-LineBreak").setAttribute("style","clear:both");
	document.getElementById("userLibraryWrapper").setAttribute("style","display:block");
}


function showUserLibrary(libData){
	var parentElement = document.getElementById("userLibrary");

	libData.forEach(function(libEntry){
		var div = createDiv(null, "animeEntry");
		div.innerHTML = libEntry.anime.title;

		parentElement.appendChild(div);
	});
}

function sortLibrary(){
	
}

function calculateLife(time){
	var months = time/60/24/30;
	var days = 30*(months - Math.floor(months));
	var hours = 24*(days - Math.floor(days));
	var minutes = 60*(hours - Math.floor(hours));

	var timeStr = Math.floor(months) + " months, " + Math.floor(days) + " days, " + Math.floor(hours) + " hours and " + Math.floor(minutes) + " minutes";

	return timeStr;
}

function formatAnimeTitle(anime){
    var pieces = anime.split("-");
    for(var i = 0; i < pieces.length; i++){
        var j = pieces[i].charAt(0).toUpperCase();
        pieces[i] = j + pieces[i].substr(1);
    }
    return pieces.join(" ");
}

$("#username").keyup(function(event){
    if(event.keyCode == 13){
    	getUser();
    }
});